const app = new Vue({
    el: '#app',
    data: {
        mensaje: 'Hola soy Samsara',
        contador: 0
    },
    computed: {
        invertido () {
            /* 
                split permite convertir en array y recibe un valor que se usará como separador
                reverse, permite revertir los valores de un array
                join permite juntar un array en un solo texto y toma un separador
            */
            return this.mensaje.split('').reverse().join('');
        },
        color () {
            return {
                'bg-success' : this.contador <= 10,
                'bg-warning' : this.contador > 10 && this.contador <20,
                'bg-danger' : this.contador >=20
            }
        }
    }
})